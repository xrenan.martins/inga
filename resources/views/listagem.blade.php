@include('header')

<div class="container">
    <div class="col-12">
        <div class="jumbotron text-center">
            <h1 class="display-4"><b>Colaboradores com tarefas</b></h1>
        </div>
    </div>
    @if (session('message'))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {{ session('message') }}
    </div>
    @endif
    <div class="col-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Listagem</li>
            </ol>
        </nav>
    </div>
    <div class="col-12">
        <h4 class="text-center"><b>LISTAGEM DE COLABORADORES COM TAREFAS ({{$total}})</b></h4>
    </div>
    <div class="col-md-2 offset-md-10">
        <a class="btn btn-light" href="{{route('vinculatarefas.index')}}" class="btn btn-default btn-sm pull-right">
            <span class="fa fa-plus"></span> Adicionar</a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Clientes</th>
                    <th>Pessoas</th>
                    <th>Atividades</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tarefas as $tarefa)
                <tr>
                    @foreach($clientes as $cliente_item)
                        @if($tarefa->clientes_id == $cliente_item['id'])
                            <td>{{$cliente_item['nome']}}</td>
                        @endif
                    @endforeach
                    @foreach($pessoas as $pessoa_item)
                        @if($tarefa->pessoas_id == $pessoa_item['id'])
                            <td>{{$pessoa_item['nome']}}</td>
                        @endif
                    @endforeach
                    @foreach($atividades as $atividade_item)
                        @if($tarefa->atividades_id == $atividade_item['id'])
                            <td>{{$atividade_item['titulo']}}</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
</div>
@include('footer')
