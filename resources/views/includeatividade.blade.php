@include('header')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="jumbotron text-center">
                <h1 class="display-4"><b>Incluir</b></h1>
            </div>
        </div>
    </div>
    @if (session('message'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('message') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">Listagem</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Incluir</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h4 class="text-center"><b>CADASTRO DE ATIVIDADE</b></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form method="post" action="{{route('includeatividade.store')}}" enctype="multipart/form-data">
                {{ csrf_field() }}


                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="titulo">Título</label>
                            <input type="text" name="titulo" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="descritivo">Descritivo</label>
                            <input type="text" name="descritivo" class="form-control" required>
                        </div>
                    </div>
                </div>

                @if ($errors->any())
                    <div class="row">

                        <div class="offset-md-6 col-6">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-3 offset-md-9">
                        <button type="reset" class="btn btn-default">
                            Limpar
                        </button>
                        <button type="submit" class="btn btn-warning" id="black">
                            Cadastrar
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <div class="clear"></div>
</div>
@include('footer')
