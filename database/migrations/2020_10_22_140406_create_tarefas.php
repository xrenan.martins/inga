<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarefas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarefas', function (Blueprint $table) {
            $table->unsignedBigInteger('clientes_id');
            $table->unsignedBigInteger('pessoas_id');
            $table->unsignedBigInteger('atividades_id');
            $table->foreign('clientes_id')->references('id')->on('clientes');
            $table->foreign('pessoas_id')->references('id')
                            ->on('pessoas')->onDelete('cascade');
            $table->foreign('atividades_id')->references('id')
                            ->on('atividades')->onDelete('cascade');;
            $table->primary(['clientes_id', 'pessoas_id', 'atividades_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tarefas');
    }
}
