<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $fillable = ['nome', 'data_nascimento', 'clientes_id'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table = 'pessoas';
}
