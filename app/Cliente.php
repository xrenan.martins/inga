<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['nome', 'endereco'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table = 'clientes';

    public function Pessoas(){
        return $this->belongsToMany(Pessoa::class, 'tarefas');
    }
    public function Atividades(){
        return $this->belongsToMany(Atividade::class, 'tarefas');
    }
}
