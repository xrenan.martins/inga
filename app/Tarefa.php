<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarefa extends Model
{
    protected $fillable = ['clientes_id', 'pessoas_id', 'atividades_id'];
    protected $guarded = ['clientes_id', 'pessoas_id', 'atividades_id', 'created_at', 'updated_at'];
    protected $table = 'tarefas';

    public function Clientes(){
        return $this->belongsToMany(Cliente::class, 'tarefas');
    }

    public function Pessoas(){
        return $this->belongsToMany(Pessoa::class, 'tarefas');
    }
    public function Atividades(){
        return $this->belongsToMany(Atividade::class, 'tarefas');
    }
}
