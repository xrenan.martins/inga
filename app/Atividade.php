<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    protected $fillable = ['titulo', 'descritivo'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table = 'atividades';

    public function Clientes(){
        return $this->belongsToMany(Cliente::class, 'tarefas');
    }
    public function Atividades(){
        return $this->belongsToMany(Atividade::class, 'tarefas');
    }
}
