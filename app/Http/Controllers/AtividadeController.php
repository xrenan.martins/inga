<?php

namespace App\Http\Controllers;

use App\Atividade;
use Validator;
use Illuminate\Http\Request;

class AtividadeController extends Controller
{
    public function index() {
        return view('includeatividade');
    }

    public function store(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'titulo' => 'required',
            'descritivo' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $atividades = Atividade::create($input);
        return $this->sendResponse($atividades->toArray(), 'Atividade salva com sucesso.');

    }
}
