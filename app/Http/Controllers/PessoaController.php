<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Pessoa;
use Validator;
use Illuminate\Http\Request;

class PessoaController extends BaseController
{
    public function index() {
        $clientes = Cliente::all();
        return view('includepessoa',compact('clientes'));
    }

    public function store(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'nome' => 'required',
            'data_nascimento' => 'required',
            'clientes_id' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $pessoas = Pessoa::create($input);
        return $this->sendResponse($pessoas->toArray(), 'Pessoa salva com  sucesso.');

    }
}
