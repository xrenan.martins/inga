<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use Validator;

class ClienteController extends BaseController
{
    public function index() {
        return view('includecliente');
    }

    public function store(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'nome' => 'required',
            'endereco' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $clientes = Cliente::create($input);
        return $this->sendResponse($clientes->toArray(), 'Cliente salvo com  sucesso.');

    }
}
