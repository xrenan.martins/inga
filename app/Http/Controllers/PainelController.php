<?php

namespace App\Http\Controllers;

use App\Atividade;
use App\Cliente;
use App\Pessoa;
use App\Tarefa;
use Illuminate\Http\Request;

class PainelController extends Controller
{
    public function index() {
        $tarefas = Tarefa::all();
        $clientes = Cliente::all();
        $pessoas = Pessoa::all();
        $atividades = Atividade::all();
        $total = Tarefa::all()->count();
        return view('listagem', compact( 'total', 'clientes', 'pessoas', 'atividades', 'tarefas'));
    }
}
