<?php

namespace App\Http\Controllers;

use App\Atividade;
use App\Cliente;
use App\Pessoa;
use App\Tarefa;
use Validator;
use Illuminate\Http\Request;

class TarefaController extends Controller
{
    public function index() {
        $pessoas = Pessoa::all();
        $atividades = Atividade::all();
        return view('vinculatarefas',compact('pessoas', 'atividades'));
    }

    public function store(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'pessoas_id' => 'required',
            'atividades_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $pessoa = Pessoa::findOrFail($input['pessoas_id']);
        $cliente = Cliente::findOrFail($pessoa['clientes_id']);
        $data = [
            "clientes_id" => $cliente->id,
            "pessoas_id" => $input['pessoas_id'],
            "atividades_id" => $input['atividades_id'],
        ];

        $tarefa = Tarefa::updateOrCreate($data);

        return redirect()->route('vinculatarefas.index')->with('message', 'Tarefa criada com sucesso!');
    }
}
